/*
 * @title App
 * @description Application entry point
 */

// Modules
import carousel from '../modules/carousel/owl';
import accordion from '../modules/accordion/accordion';
import navbar from './components/navbar';

document.addEventListener('DOMContentLoaded', function() {
  carousel();
  navbar();
  accordion();
})

