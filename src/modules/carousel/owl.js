/**
 * Carousel
 * https://owlcarousel2.github.io/
 */

// eslint-disable-next-line no-unused-vars
import owl from '../../../node_modules/owl.carousel/dist/owl.carousel.min'

export function carousel() {
  var carousel = $('.owl-carousel');

  if(carousel.length > 0){
    carousel.owlCarousel({
      items: 1,
      nav: true,
      dots: true
    });
  }
}

export default carousel;